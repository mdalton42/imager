const express = require('express')
const sharp = require('sharp');
const app = express();
const imagemin = require('imagemin');
const imageminMozjpeg = require('imagemin-mozjpeg');


// (async () => {
// 	await imagemin(['images/*.jpg'], 'build/images', {
// 		use: [
// 			imageminMozjpeg()
// 		]
// 	});

// 	console.log('Images optimized');
// })();

// let smallImage = async () => {
//   await Jimp.read('imgs/test.jpg')
//   .then(image => (image.resize(256,256)))
//   .then(im => {
//     return imagemin.buffer(im.bitmap.data, {use: plugins})
//   }).then(im => {
//     console.log(im);
//     res.send(im);
//   })
// }

function resizedImage(path, resize){
  return sharp(`imgs${path}`)
          .resize({
            width: +resize.width,
            height: +resize.height,
            fit: resize.fit,
          })
          .toBuffer();
}

function smallify(buffer, quality){
  return imagemin.buffer(buffer, {use: [
    imageminMozjpeg({
      quality: quality,
      arithemtic: false,
      progressive: true,
      smooth: 0,
      quant_table: 3,
    })
  ]})
}

app.get('*', async (req, res) => {
  const path = req.path;
  console.log(req.path);
  const width = req.query.width || false;
  const height = req.query.height || false;
  const fit = req.query.fit || 'outside';
  const quality = +req.query.quality || 75;
  
  let resizedBuffer = await resizedImage(path, {width: width, height: height, fit: fit});
  let smallBuffer = await smallify(resizedBuffer, quality);

  //console.log(resizedBuffer);
  //console.log('----------');


  res.contentType('image/jpeg');
  res.send(smallBuffer);

  //res.write(buffer,'binary');
  //res.end(undefined,'binary');


});

app.listen(8000, () => {
  console.log('Example app listening on port 8000!')
});